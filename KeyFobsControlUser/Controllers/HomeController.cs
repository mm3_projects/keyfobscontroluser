﻿using KeyFobsControlUser.Models;
using System.Web.Mvc;
using System.Linq;
using System.Data;
using System;
using PolisportAPI.ModeloEstruturas.M3Mattec;
using KeyFobsControlUser.Auth;
using PolisportAPI.Logic;

namespace KeyFobsControlUser.Controllers {
    public class HomeController : Controller {
        // GET: Home
        [HttpGet]
        public ActionResult Index(string mensagem, bool erro = false) {
            if (!string.IsNullOrEmpty(mensagem))
            {
                if (erro)
                {
                    ViewBag.Erro = mensagem;
                }
                else
                {
                    ViewBag.Info = mensagem;
                }
            }

            return View();
        }

        public ActionResult ListReasons(string readkeyfob) {
            var mattecLogic = new PolisportAPI.Logic.MattecLogic();

            if (!mattecLogic.CheckIfOperatorExist(readkeyfob, " "))
            {
                return RedirectToAction("Index", new { mensagem = "Utilizador não registado!", erro = true });  
            }

            var operadoresLogados = mattecLogic.OperadoresLogados().Rows.OfType<DataRow>().Select(x => x["KeyFob"].ToString().Trim()).ToList();
            if (operadoresLogados.Contains(readkeyfob.Trim()))
            {
               return RedirectToAction("Index", new { mensagem = "Utilizador já logado nas linhas. Faça logout nas linhas e volte para fazer login na manutenção.", erro = true }); 
            }

            var mattecLoginMaintenanceLogic = new PolisportAPI.Logic.MattecLoginMaintenanceLogic();
            if (mattecLoginMaintenanceLogic.CheckIfAlreadyLoggedMaintenance(readkeyfob))
            {
                try {
                    mattecLoginMaintenanceLogic.Logout(readkeyfob);
                    return RedirectToAction("Index", new { mensagem = "Logout feito com sucesso", erro = false }); 
                }
                catch (Exception)
                {
                    return RedirectToAction("Index", new { mensagem = "Ocorreu um erro ao fazer logout. Contacte o administrador.", erro = true }); 
                }
            }
         
            var vma = new ViewModel_Reasons();
            var reasonsTypeLogic = new MattecReasonsTypeLogic();
            var reasonsLogic = new MattecReasonsLogic();

            vma.Reasons = reasonsLogic.GetAll();
            vma.ReasonsType = reasonsTypeLogic.GetAll();
            vma.KeyFob = readkeyfob;

            return View(vma);
        }

        public ActionResult SetReasons(string reason, string reasonType, string keyfob) {
            if (string.IsNullOrEmpty(reason) || string.IsNullOrEmpty(reasonType))
            {
                return RedirectToAction("Index", new { mensagem = "Não selecionou a razão" , erro = true });
            }

            var reasonsTypeLogic = new MattecReasonsTypeLogic();
            var reasonsType = reasonsTypeLogic.GetAll().ToDictionary(x => x.Type.ToLower().Trim(), x => x.Reason_Type_ID);
            var id = reasonsType[reasonType.ToLower().Trim()];

            try {
                var mattecLoginMaintenanceLogic = new PolisportAPI.Logic.MattecLoginMaintenanceLogic();

                var mattecLogic = new PolisportAPI.Logic.MattecLogic();
                var operatordata = mattecLogic.GetByKeyfob(keyfob, " ");

                var reasonsLogic = new MattecReasonsLogic();
                var reasonID = reasonsLogic.GetAll().Where(x => x.Reason.Trim().ToLower() == reason.Trim().ToLower()).FirstOrDefault().Reason_ID;

                var poco = new MattecLoginMaintenancePoco()
                {
                    Facility = operatordata.NumeroOperador.Split('_')[1],
                    Login_Date = DateTime.Now,
                    Operator_Desc = operatordata.Nome,
                    Operator_ID = keyfob.Trim(),
                    Reason_Desc = reason.Trim(),
                    Reason_ID = reasonID
                };

                mattecLoginMaintenanceLogic.Insert(poco);
            }
            catch (Exception ex)
            {
                return RedirectToAction("Index", new { mensagem = "Ocorreu um erro ao fazer login. Contacte o administrador.", erro = true }); 
            }

            return RedirectToAction("Index", new { mensagem = "Login feito com sucesso", erro = false });
        }
    }
}