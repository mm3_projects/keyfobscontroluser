﻿using KeyFobsControlUser.Auth;
using KeyFobsControlUser.Models;
using System;
using System.Web.Mvc;
using System.Collections.Generic;
using System.Linq;
using PolisportAPI.Logic;
using PolisportAPI.ModeloEstruturas.M3Mattec;

namespace KeyFobsControlUser.Controllers
{
    public class AdminController : Controller
    {
        public ActionResult UsersAdmin(string mensagem)
        {
            if (Session[SessionVariables.Variaveis.UserAuthentication.Nome] == null)
            {
                return RedirectToAction("Valida", "Login");
            }

            if (!string.IsNullOrEmpty(mensagem))
            {
                ViewBag.Erro = mensagem;
            }

            var pocoLogin = (POCO_Login)Session[SessionVariables.Variaveis.UserAuthentication.Nome];
            var logic = new MattecLogic();

            var vma = new ViewModel_Admin();
            vma.Operadores = logic.GetAllByFacility(pocoLogin.Facility);
            return View(vma);
        }

        [HttpGet]
        public ActionResult NovoActualizaAdmin(string keyfob, string nome, string npolisport, string activo, string update)
        {
            if (Session[SessionVariables.Variaveis.UserAuthentication.Nome] == null)
            {
                return RedirectToAction("Valida", "Login");
            }

            if (keyfob == null || nome == null || npolisport == null || activo == null)
            {
                return RedirectToAction("UsersAdmin");
            }

            var pocoLogin = (POCO_Login)Session[SessionVariables.Variaveis.UserAuthentication.Nome];
            var poco = new Operador()
            {
               KeyFob = keyfob.Trim(),
               Nome = nome.Trim(),
               NumeroOperador = npolisport.Trim() + "_" + pocoLogin.Facility.Trim(),
               Activo = activo
            };

            var logic = new MattecLogic();
            if (!logic.CheckIfOperatorExist(poco.KeyFob, pocoLogin.Facility.Trim()))
            {
                logic.CreateOperator(poco);
            }
            else
            {
                if (!bool.Parse(update))
                {
                    return RedirectToAction("UsersAdmin", new { mensagem = "Já existe um operador com este keyfob" });
                }
                else
                {
                    logic.UpdateOperator(poco);
                }
            }

            return RedirectToAction("UsersAdmin");
        }

        public ActionResult ReasonsAdmin(string mensagem)
        {
            if (Session[SessionVariables.Variaveis.UserAuthentication.Nome] == null)
            {
                return RedirectToAction("Valida", "Login");
            }

            if (!string.IsNullOrEmpty(mensagem))
            {
                ViewBag.Erro = mensagem;
            }

            var reasonsTypeLogic = new MattecReasonsTypeLogic();
            var reasonsLogic = new MattecReasonsLogic();

            var vma = new ViewModel_Reasons();
            vma.Reasons = reasonsLogic.GetAll();
            vma.ReasonsType = reasonsTypeLogic.GetAll();

            return View(vma);
        }

        [HttpGet]
        public ActionResult NovoActualizaRazao(string razao, string tiporazao, string reasonid)
        {
            if (Session[SessionVariables.Variaveis.UserAuthentication.Nome] == null)
            {
                return RedirectToAction("Valida", "Login");
            }

            if (razao == null || tiporazao == null)
            {
                return RedirectToAction("ReasonsAdmin");
            }

            var reasonsTypeLogic = new MattecReasonsTypeLogic();
            var reasonTypeID = reasonsTypeLogic.GetAll().Where(x => x.Type.ToLower().Trim() == tiporazao.ToLower().Trim()).FirstOrDefault().Reason_Type_ID;

            var poco = new Reasons()
            {
                Reason = razao,
                Reason_Type_ID = reasonTypeID
            };

            var logic = new MattecReasonsLogic();

            if (!logic.CheckIfReasonExist(poco))
            {
                logic.CreateReason(poco);
            }
            else
            {
                return RedirectToAction("ReasonsAdmin", new { mensagem = "Já existe uma razão criada com o mesmo nome" });
            }

            return RedirectToAction("ReasonsAdmin");
        }

        public ActionResult Delete(string reasonid)
        {
            if (Session[SessionVariables.Variaveis.UserAuthentication.Nome] == null)
            {
                return RedirectToAction("Valida", "Login");
            }

            if (reasonid == null)
            {
                return RedirectToAction("ReasonsAdmin");
            }

            var logic = new MattecReasonsLogic();
            logic.Delete(reasonid);

            return RedirectToAction("ReasonsAdmin");
        }
    }
}