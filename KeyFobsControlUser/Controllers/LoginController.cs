﻿using KeyFobsControlUser.Auth;
using KeyFobsControlUser.Models;
using System;
using System.Web.Mvc;

namespace KeyFobsControlUser.Controllers
{
    public class LoginController : Controller
    {
        public ActionResult Valida(POCO_Login utilizador)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            try
            {

                utilizador.HasAcess();
                if (!utilizador.UserLoginOK)
                {
                    ViewBag.Erro = "Credenciais inválidas/Necessita de permissão";
                    return View();
                }

                Session[SessionVariables.Variaveis.UserAuthentication.Nome] = utilizador;
                return RedirectToAction("UsersAdmin", "Admin");
            }
            catch (Exception ex)
            {
                ViewBag.Erro = ex.Message;
                return View();
            }
        }

        public ActionResult ValidLogout()
        {
            try
            {
                Session[SessionVariables.Variaveis.UserAuthentication.Nome] = null;
                HttpContext.Session.Abandon();
                return RedirectToAction("Index", "Home");
            }
            catch (Exception ex)
            {
                ViewBag.Erro = ex.Message;
                return View();
            }
        }
    }
}