﻿namespace KeyFobsControlUser.Auth {
    public class SessionVariables {
        public class Variaveis {
            public string Nome { get; set; }

            public static Variaveis UserAuthentication = new Variaveis() { Nome = "user_login_authentication" };
        }
    }
}