﻿using PolisportAPI.ModeloEstruturas.M3Mattec;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KeyFobsControlUser.Models
{
    public class ViewModel_Reasons
    {
        public List<ReasonsType> ReasonsType { get; set; }
        public List<ReasonsModel> Reasons { get; set; }
        public string KeyFob { get; set; }
    }
}