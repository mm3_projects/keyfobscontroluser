﻿using System;
using System.Web;
using System.ComponentModel.DataAnnotations;
using KeyFobsControlUser.Auth;
using PolisportAPI.Logic;

namespace KeyFobsControlUser.Models
{
    public class POCO_Login
    {
        [Key]
        public int ID { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Insira nome de utilizador.")]
        [Display(Name="Utilizador")]
        public string User { get; set; }

        [DataType(DataType.Password)]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Insira uma password.")]
        public string Password { get; set; }

        public bool UserLoginOK { get; set; }

        public string Facility { get; set; }

        public void HasAcess() {
            try {

                var UsersAD = new PolisportAPI.LdapAuthentication(PolisportAPI.SystemConfigs.ADLink);
                var temAcesso = PolisportAPI.API.CheckADLogin(this.User.Trim(), this.Password.Trim(), UsersAD);

                if (temAcesso)
                {
                    var logic = new MattecKeyFobControlUserAdminLogic();
                    var poco = logic.GetByName(this.User.Trim());

                    if (poco == null)
                    {
                        temAcesso = false;
                    }
                    else
                    {
                        this.Facility = poco.Facility;
                    }
                }

                this.UserLoginOK = temAcesso;
            } catch (Exception ex) {
                throw ex;
            }
        }
    }
}