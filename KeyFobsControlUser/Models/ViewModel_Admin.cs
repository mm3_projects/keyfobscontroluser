﻿using PolisportAPI.ModeloEstruturas.M3Mattec;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KeyFobsControlUser.Models
{
    public class ViewModel_Admin
    {
        public List<Operador> Operadores { get; set; }
        public Operador NovoLoginOperador { get; set; }
    }
}