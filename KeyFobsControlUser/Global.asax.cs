﻿using System;
using System.Web.Mvc;
using System.Web.Routing;

namespace KeyFobsControlUser
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
        }

        void Session_End(Object sender, EventArgs E)
        {
            Session.Clear();
        }
    }
}
